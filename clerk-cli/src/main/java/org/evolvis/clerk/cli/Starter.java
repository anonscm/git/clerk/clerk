/**
 * 
 */
package org.evolvis.clerk.cli;

import java.io.File;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.evolvis.clerk.core.beans.Addressee;
import org.evolvis.clerk.core.beans.Letter;
import org.evolvis.clerk.core.beans.LetterTemplate;
import org.evolvis.clerk.core.beans.SenderIdentity;
import org.evolvis.clerk.core.config.ClerkConfiguration;
import org.evolvis.clerk.core.engine.impl.ClerkWorkflow;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class Starter {

	public static void main(String[] args) {

		Options options = new Options();

		options.addOption("help", false, "print this message");
		options.addOption("a", true, "set the addressee of the letter");
		options.addOption("source", true, "the latex template to use");

		CommandLineParser parser = new GnuParser();

		try {
			CommandLine cmd = parser.parse(options, args);

			if(cmd.hasOption("help")) {

				HelpFormatter formatter = new HelpFormatter();
				formatter.setLongOptPrefix("--");
				formatter.printHelp( "clerk-cli", options );
				System.exit(0);
			}
			
			ClerkConfiguration configuration = new ClerkConfiguration();
			
			LetterTemplate template = new LetterTemplate("Brief", new File(""));
			
			Addressee addressee = new Addressee();
			SenderIdentity sender = new SenderIdentity();
			Letter text = new Letter();
			
			ClerkWorkflow workflow = new ClerkWorkflow(configuration, template, addressee, sender, text);
			
			workflow.run();
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
