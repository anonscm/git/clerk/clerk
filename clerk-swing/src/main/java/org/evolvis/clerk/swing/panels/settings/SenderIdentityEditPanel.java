/**
 * 
 */
package org.evolvis.clerk.swing.panels.settings;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextField;

import org.evolvis.clerk.core.beans.SenderIdentity;
import org.evolvis.clerk.swing.panels.AbstractEntityEditPanel;
import org.evolvis.clerk.swing.panels.AddressPanel;
import org.evolvis.clerk.swing.utils.I18n;
import org.evolvis.clerk.swing.utils.SwingUtils;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.factories.ButtonBarFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class SenderIdentityEditPanel extends AbstractEntityEditPanel<SenderIdentity> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3426981056634576951L;
	
	private JButton submitButton;
	private JButton cancelButton;
	private final static String ENTITY_NAME = I18n.getMessage("senderIdentity");
	
	private Action submitAction;
	private Action cancelAction;
	
	private JTextField identityNameField;
	private AddressPanel addressPanel;
	private JTextField senderPlaceField;
	private JTextField signatureField;
	private JTextField phoneField;
	private JTextField faxField;
	private JTextField emailField;
	private JTextField wwwField;
//	private JComboBox defaultTemplateField;

	public SenderIdentityEditPanel() {
		
		setLayout(new BorderLayout());

		FormLayout layout = new FormLayout(
				"max(pref;40dlu), 3dlu, max(pref;150dlu):grow", // columns
//		"p, 10dlu, p, 3dlu, p, 10dlu, p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p, 10dlu, p, 3dlu, p, 15dlu, p"); // rows
		"p, 10dlu, p, 3dlu, p, 10dlu, p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p, 15dlu, p"); // rows
				

		PanelBuilder builder = new PanelBuilder(layout);
		builder.setDefaultDialogBorder();
		CellConstraints cc = new CellConstraints();
		
		builder.add(identityNameField = new JTextField(), cc.xy(3, 1));
		builder.addLabel(I18n.getMessage("identityName"), cc.xy(1, 1)).setLabelFor(identityNameField);
		
		builder.addSeparator(I18n.getMessage("identityAddress"), cc.xyw(1, 3, 3));
		
		builder.add(addressPanel = new AddressPanel(), cc.xyw(1, 5, 3));
		
		builder.addSeparator(I18n.getMessage("identityDetails"), cc.xyw(1, 7, 3));
		
		builder.add(senderPlaceField = new JTextField(), cc.xy(3, 9));
		builder.addLabel(I18n.getMessage("identityPlace"), cc.xy(1, 9)).setLabelFor(senderPlaceField);
		
		builder.add(signatureField = new JTextField(), cc.xy(3, 11));
		builder.addLabel(I18n.getMessage("identitySignature"), cc.xy(1, 11)).setLabelFor(signatureField);
		
		builder.add(phoneField = new JTextField(), cc.xy(3, 13));
		builder.addLabel(I18n.getMessage("identityPhone"), cc.xy(1, 13)).setLabelFor(phoneField);
		
		builder.add(faxField = new JTextField(), cc.xy(3, 15));
		builder.addLabel(I18n.getMessage("identityFax"), cc.xy(1, 15)).setLabelFor(faxField);
		
		builder.add(emailField = new JTextField(), cc.xy(3, 17));
		builder.addLabel(I18n.getMessage("identityEmail"), cc.xy(1, 17)).setLabelFor(emailField);
		
		builder.add(wwwField = new JTextField(), cc.xy(3, 19));
		builder.addLabel(I18n.getMessage("identityWWW"), cc.xy(1, 19)).setLabelFor(wwwField);
		
//		builder.addSeparator("Templates", cc.xyw(1, 21, 3));
		
//		builder.add(defaultTemplateField = new JComboBox(), cc.xy(3, 23));
//		builder.addLabel("Default Template", cc.xy(1, 23)).setLabelFor(defaultTemplateField);
		
		builder.add(ButtonBarFactory.buildOKCancelBar(
				submitButton = new JButton("", SwingUtils.getIcon("dialog-ok")),
				cancelButton = new JButton(I18n.getMessage("buttonCancel"), SwingUtils.getIcon("dialog-cancel"))), cc.xyw(1, 21, 3));
		
		add(builder.getPanel(), BorderLayout.CENTER);
		
		submitButton.addActionListener(new ActionListener() {

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if(submitAction != null)
					submitAction.actionPerformed(e);
				
			}
		});
		
		cancelButton.addActionListener(new ActionListener() {

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if(cancelAction != null)
					cancelAction.actionPerformed(e);
			}
		});
	}

	/**
	 * @see org.evolvis.clerk.swing.panels.AbstractEntityEditPanel#setActionName(java.lang.String)
	 */
	@Override
	public void setActionName(String actionName) {
		
		submitButton.setText(actionName);
	}

	/**
	 * @see org.evolvis.clerk.swing.panels.AbstractEntityEditPanel#setCancelAction(javax.swing.Action)
	 */
	@Override
	public void setCancelAction(Action cancelAction) {
		
		this.cancelAction = cancelAction;
	}

	/**
	 * @see org.evolvis.clerk.swing.panels.AbstractEntityEditPanel#setSubmitAction(javax.swing.Action)
	 */
	@Override
	public void setSubmitAction(Action submitAction) {
		
		this.submitAction = submitAction;
	}

	/**
	 * @see org.evolvis.clerk.swing.panels.AbstractEntityEditPanel#setEntity(java.lang.Object)
	 */
	@Override
	public void setEntity(SenderIdentity identity) {
				
		identityNameField.setText(identity.getIdentityName());
		addressPanel.setAddress(identity);
		senderPlaceField.setText(identity.getCreationPlace());
		signatureField.setText(identity.getSignature());
		phoneField.setText(identity.getPhone());
		faxField.setText(identity.getFax());
		emailField.setText(identity.getEmail());
		wwwField.setText(identity.getWWW());
		
		// TODO set default template
	}

	/**
	 * @see org.evolvis.clerk.swing.panels.AbstractEntityEditPanel#clear()
	 */
	@Override
	public void clear() {
	
		identityNameField.setText("");
		
		addressPanel.clear();
		
		senderPlaceField.setText("");
		
		signatureField.setText("");
		
		phoneField.setText("");
		
		faxField.setText("");
		
		emailField.setText("");
		
		wwwField.setText("");
		
		// TODO clear default template
	}

	@Override
	public SenderIdentity getEntity() {
		
		SenderIdentity identity = new SenderIdentity(identityNameField
				.getText(), addressPanel.getAddress(), null, senderPlaceField
				.getText(), signatureField.getText(), phoneField.getText(),
				faxField.getText(), emailField.getText(), wwwField.getText());
		
		return identity;
	}
}
