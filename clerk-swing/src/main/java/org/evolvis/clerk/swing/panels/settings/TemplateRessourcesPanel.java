/**
 * 
 */
package org.evolvis.clerk.swing.panels.settings;

import javax.swing.JDialog;

import org.evolvis.clerk.core.beans.LetterTemplateRessource;
import org.evolvis.clerk.swing.utils.I18n;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class TemplateRessourcesPanel extends AbstractEntityManagementPanel<LetterTemplateRessource> {


	/**
	 * 
	 */
	private static final long serialVersionUID = -1068580237690722427L;

	public TemplateRessourcesPanel(JDialog parent) {
		
		super(parent, new TemplateRessourceEditPanel(), I18n.getMessage("addTemplateRessource"), I18n.getMessage("changeTemplateRessource"), I18n.getMessage("removeTemplateRessource"));
	}
}
