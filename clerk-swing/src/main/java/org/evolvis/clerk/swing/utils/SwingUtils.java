/**
 * 
 */
package org.evolvis.clerk.swing.utils;

import java.awt.AWTKeyStroke;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Frame;
import java.awt.KeyboardFocusManager;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.text.ParseException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JTextArea;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.MaskFormatter;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.factories.ButtonBarFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class SwingUtils {

	public final static String ICON_PATH = "/org/evolvis/clerk/swing/icons/";

	public static MaskFormatter getPostalCodeFormatter(Locale locale) {

		if(locale == null)
			locale = Locale.getDefault();

		// Currently only a formatter for German postal codes is defined
		MaskFormatter postalCodeFormatter = new MaskFormatter();
		postalCodeFormatter.setValidCharacters("1234567890");
		try {
			postalCodeFormatter.setMask("*****");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		postalCodeFormatter.setAllowsInvalid(false);

		return postalCodeFormatter;
	}

	public static Icon getIcon(String name) {

		return getIcon(name, 22);
	}

	public static Icon getIcon(String name, int resolution) {

		return new ImageIcon(Toolkit.getDefaultToolkit().getImage(SwingUtils.class.getResource(ICON_PATH + resolution + "x" + resolution + "/"+ name + ".png")));
	}

	public static void makeTextAreaTabFocusTraversal(JTextArea editorPane) {

		Set<AWTKeyStroke> forwardKeys  = editorPane.getFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS);
		Set<AWTKeyStroke> backwardKeys = editorPane.getFocusTraversalKeys(KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS);

		// check that we WANT to modify current focus traversal keystrokes
		if (forwardKeys.size() != 1 || backwardKeys.size() != 1) return;
		final AWTKeyStroke fks = forwardKeys.iterator().next();
		final AWTKeyStroke bks = backwardKeys.iterator().next();
		final int fkm = fks.getModifiers();
		final int bkm = bks.getModifiers();
		final int ctrlMask      = KeyEvent.CTRL_MASK+KeyEvent.CTRL_DOWN_MASK;
		final int ctrlShiftMask = KeyEvent.SHIFT_MASK+KeyEvent.SHIFT_DOWN_MASK+ctrlMask;
		if (fks.getKeyCode() != KeyEvent.VK_TAB || (fkm & ctrlMask) == 0 || (fkm & ctrlMask) != fkm)
		{       // not currently CTRL+TAB for forward focus traversal
			return;
		}
		if (bks.getKeyCode() != KeyEvent.VK_TAB || (bkm & ctrlShiftMask) == 0 || (bkm & ctrlShiftMask) != bkm)
		{       // not currently CTRL+SHIFT+TAB for backward focus traversal
			return;
		}

		// bind our new forward focus traversal keys
		Set<AWTKeyStroke> newForwardKeys = new HashSet<AWTKeyStroke>(1);
		newForwardKeys.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_TAB,0));
		editorPane.setFocusTraversalKeys(
				KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS,
				Collections.unmodifiableSet(newForwardKeys)
		);
		// bind our new backward focus traversal keys
		Set<AWTKeyStroke> newBackwardKeys = new HashSet<AWTKeyStroke>(1);
		newBackwardKeys.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_TAB,KeyEvent.SHIFT_MASK+KeyEvent.SHIFT_DOWN_MASK));
		editorPane.setFocusTraversalKeys(
				KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS,
				Collections.unmodifiableSet(newBackwardKeys)
		);
	}
	
	public static String showDirectorySelectionDialog(Component parent, String preset) {
		
		JFileChooser fileChooser = new JFileChooser(preset);
		
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		
		int returnVal = fileChooser.showDialog(parent, "Choose");
		
		if(returnVal == JFileChooser.APPROVE_OPTION)
			return fileChooser.getSelectedFile().getAbsolutePath();
		
		return null;
	}
	
	public static String showFileSelectionDialog(Component parent, String preset, FileFilter fileFilter) {
		
		JFileChooser fileChooser = new JFileChooser(preset);
		
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		
		if(fileFilter != null)
			fileChooser.setFileFilter(fileFilter);
		
		int returnVal = fileChooser.showDialog(parent, "Choose");
		
		if(returnVal == JFileChooser.APPROVE_OPTION)
			return fileChooser.getSelectedFile().getAbsolutePath();
		
		return null;
	}

	public static void showErrorDialog(Frame parent, Exception excp) {

		final JDialog dialog = new JDialog(parent, "Error", true);
		
		dialog.getContentPane().setLayout(new BorderLayout());

		FormLayout layout = new FormLayout(
				"pref", // columns
		"pref, 10dlu, pref"); // rows

		PanelBuilder builder = new PanelBuilder(layout);

		CellConstraints cc = new CellConstraints();
		
		builder.addLabel(excp.getLocalizedMessage(), cc.xy(1, 1));
		
		JButton closeButton;
		
		builder.add(ButtonBarFactory.buildOKBar(
				closeButton = new JButton("Close")), cc.xy(1, 3));
		
		closeButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				dialog.setVisible(false);
			}
		});
		
		dialog.getContentPane().add(builder.getPanel(), BorderLayout.CENTER);
		
		dialog.pack();
		
		dialog.setLocationRelativeTo(parent);
		
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
		dialog.setVisible(true);
	}
}
