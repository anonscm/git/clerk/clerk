/**
 * 
 */
package org.evolvis.clerk.swing;

import javax.swing.JFrame;
import javax.swing.UIManager;

import org.evolvis.clerk.core.config.ClerkConfiguration;
import org.evolvis.clerk.swing.panels.CreateLetterPanel;
import org.evolvis.clerk.swing.utils.I18n;

import com.jgoodies.looks.plastic.Plastic3DLookAndFeel;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class Starter {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		try {
//			Plastic3DLookAndFeel.setCurrentTheme(new LightGray());
			UIManager.setLookAndFeel(new Plastic3DLookAndFeel());
			
		} catch (Exception e) {
			throw new RuntimeException("Failed to set Look And Feel", e);
		}

		JFrame mainFrame = new JFrame(I18n.getMessage("mainFrameTitle"));
		
		// Load configuration
		ClerkConfiguration configuration = ClerkConfiguration.load();

		mainFrame.getContentPane().add(new CreateLetterPanel(mainFrame, configuration));

		mainFrame.pack();

		mainFrame.setLocationRelativeTo(null);

		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setVisible(true);
	}

}
