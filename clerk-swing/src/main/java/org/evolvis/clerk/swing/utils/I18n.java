/**
 * 
 */
package org.evolvis.clerk.swing.utils;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class I18n {

	private static ResourceBundle MESSAGES = ResourceBundle.getBundle("org.evolvis.clerk.swing.messages");
	
	public static String getMessage(String messageID) {
		
		try {
			
			return MESSAGES.getString(messageID);
			
		} catch (MissingResourceException excp) {
			
			return "MISSING: "+messageID;
		}
	}
	
	public static String getMessage(String messageID, String... messageParts) {
		
		String message = getMessage(messageID);
		
		for(int i = 0; i < messageParts.length; i++)
			message = message.replaceAll("\\{"+i+"\\}", messageParts[i]);
		
		return message;
	}
}
