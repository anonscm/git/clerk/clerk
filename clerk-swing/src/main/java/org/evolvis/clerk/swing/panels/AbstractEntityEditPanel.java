/**
 * 
 */
package org.evolvis.clerk.swing.panels;

import javax.swing.Action;
import javax.swing.JPanel;

import org.evolvis.clerk.core.beans.AbstractClerkBean;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public abstract class AbstractEntityEditPanel<T extends AbstractClerkBean> extends JPanel {

	public abstract void setActionName(String actionName);
	
	public abstract void setSubmitAction(Action submitAction);
	
	public abstract void setCancelAction(Action cancelAction);
	
	public abstract T getEntity();
	
	public abstract void setEntity(T entity);
	
	public abstract void clear();
}
