/**
 * 
 */
package org.evolvis.clerk.swing.panels.settings;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.evolvis.clerk.core.beans.AbstractClerkBean;
import org.evolvis.clerk.swing.panels.AbstractEntityEditPanel;
import org.evolvis.clerk.swing.utils.I18n;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public abstract class AbstractEntityManagementPanel<T extends AbstractClerkBean> extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8323331501532029197L;

	JButton newButton;
	JButton editButton;
	JButton deleteButton;

	AbstractEntityEditPanel<T> entityEditPanel;
	JDialog parent;
	JList entitiesList;
	String createEntityLabel;
	String editEntityLabel;
	String deleteEntityLabel;


	public AbstractEntityManagementPanel(JDialog parent, AbstractEntityEditPanel<T> entityEditPanel, String createEntityLabel, String editEntityLabel, String deleteEntityLabel) {

		setLayout(new BorderLayout());

		this.parent = parent;
		this.entityEditPanel = entityEditPanel;
		this.createEntityLabel = createEntityLabel;
		this.editEntityLabel = editEntityLabel;
		this.deleteEntityLabel = deleteEntityLabel;
		

		FormLayout layout = new FormLayout(
				"pref:grow, 5dlu, pref", // columns
		"pref, 5dlu, pref, 5dlu, pref, pref:grow"); // rows

		PanelBuilder builder = new PanelBuilder(layout);
		CellConstraints cc = new CellConstraints();

		entitiesList = new JList(new DefaultListModel());
		entitiesList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		entitiesList.setBorder(BorderFactory.createLineBorder(UIManager.getColor("Tree.selectionBorderColor")));
		entitiesList.addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {

				if(e.getValueIsAdjusting())
					return;

				Object value = entitiesList.getSelectedValue();

				editButton.setEnabled(value != null);
				deleteButton.setEnabled(value != null);
			}
		});
		entitiesList.setCellRenderer(new DefaultListCellRenderer() {

			@Override
			public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
				
				return super.getListCellRendererComponent(list, ((AbstractClerkBean)value).getDisplayName(), index, isSelected, cellHasFocus);
			}
		});

		builder.add(entitiesList, cc.xywh(1, 1, 1, 6, CellConstraints.FILL, CellConstraints.FILL));

		builder.add(newButton = new JButton(createEntityLabel), cc.xy(3, 1));

		builder.add(editButton = new JButton(editEntityLabel), cc.xy(3, 3));
		editButton.setEnabled(false);

		builder.add(deleteButton = new JButton(deleteEntityLabel), cc.xy(3, 5));
		deleteButton.setEnabled(false);

		add(builder.getPanel(), BorderLayout.CENTER);

		newButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				showEditDialog(false);
			}
		});

		editButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				showEditDialog(true);
			}
		});
		
		deleteButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				T entity = (T)entitiesList.getSelectedValue();

				int answer = JOptionPane.showConfirmDialog(AbstractEntityManagementPanel.this.parent, I18n.getMessage("deleteEntityQuestion", entity.getDisplayName()),
						AbstractEntityManagementPanel.this.deleteEntityLabel,
						JOptionPane.YES_NO_OPTION);
				
				if(answer == JOptionPane.YES_OPTION)
					((DefaultListModel)entitiesList.getModel()).removeElement(entity);
			}
		});
	}
	
	protected List<T> getEntities() {
		
		List<T> entities = new ArrayList<T>(((DefaultListModel)entitiesList.getModel()).size());
		Enumeration<?> elements = ((DefaultListModel)entitiesList.getModel()).elements();
		
		while(elements.hasMoreElements())
			entities.add((T)elements.nextElement());
		
		return entities;
	}
	
	protected void setEntities(List<T> entities) {
		
		Iterator<T> iterator = entities.iterator();
		
		while(iterator.hasNext())
			((DefaultListModel)entitiesList.getModel()).addElement(iterator.next());
	}

	private void showEditDialog(final boolean edit) {

		final JDialog dialog = new JDialog(
				parent,
				(edit ? editEntityLabel : createEntityLabel),
				true);
		
		final T currentEntity = (T)entitiesList.getSelectedValue();

		if(edit)
			entityEditPanel.setEntity(currentEntity);
		else 
			entityEditPanel.clear();

		entityEditPanel.setActionName(edit ? editEntityLabel : createEntityLabel);
		entityEditPanel.setCancelAction(new AbstractAction() {

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(ActionEvent e) {

				dialog.setVisible(false);
			}
		});

		entityEditPanel.setSubmitAction(new AbstractAction() {

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(ActionEvent e) {

				if(edit)
					((DefaultListModel)entitiesList.getModel()).removeElement(currentEntity);
				
				((DefaultListModel)entitiesList.getModel()).addElement(entityEditPanel.getEntity());
				
				dialog.setVisible(false);
			}
		});

		dialog.getContentPane().add(entityEditPanel);

		dialog.pack();

		dialog.setLocationRelativeTo(parent);

		dialog.setVisible(true);
	}
}
