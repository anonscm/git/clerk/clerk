/**
 * 
 */
package org.evolvis.clerk.swing.panels.settings;

import javax.swing.JDialog;
import javax.swing.JPanel;

import org.evolvis.clerk.core.beans.LetterTemplate;
import org.evolvis.clerk.core.config.ClerkConfiguration;
import org.evolvis.clerk.swing.utils.I18n;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class TemplatesManagementPanel extends AbstractEntityManagementPanel<LetterTemplate> implements SettingsModule {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9034412162486002764L;
	
	ClerkConfiguration configuration;

	public TemplatesManagementPanel(ClerkConfiguration configuration, JDialog parent) {
		
		super(parent, new TemplateEditPanel(parent), I18n.getMessage("addTemplate"), I18n.getMessage("changeTemplate"), I18n.getMessage("removeTemplate"));
		
		this.configuration = configuration;	
		
		setEntities(configuration.getLetterTemplates());
	}

	@Override
	public void applyConfiguration() {
		
		configuration.setLetterTemplates(getEntities());
	}

	@Override
	public JPanel getPanel() {
		
		return this;
	}
}
