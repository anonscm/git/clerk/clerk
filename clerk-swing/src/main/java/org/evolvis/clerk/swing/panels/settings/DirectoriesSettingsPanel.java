/**
 * 
 */
package org.evolvis.clerk.swing.panels.settings;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.evolvis.clerk.core.config.ClerkConfiguration;
import org.evolvis.clerk.swing.utils.I18n;
import org.evolvis.clerk.swing.utils.SwingUtils;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class DirectoriesSettingsPanel extends JPanel implements SettingsModule {
	
	JTextField workingDirectoryField;
	JTextField archiveDirectoryField;
	
	JButton workingDirectoryButton;
	JButton archiveDirectoryButton;
	
	ClerkConfiguration configuration;
	
	public DirectoriesSettingsPanel(ClerkConfiguration configuration) {
		
		this.configuration = configuration;
		
		setLayout(new BorderLayout());

		FormLayout layout = new FormLayout(
				"pref, 3dlu, pref:grow, 3dlu, pref", // columns
		"pref, 3dlu, pref, 10dlu, pref, 3dlu, pref"); // rows

		PanelBuilder builder = new PanelBuilder(layout);
		CellConstraints cc = new CellConstraints();

		builder.addSeparator(I18n.getMessage("workingDirectorySeparator"), cc.xyw(1, 1, 3));

		builder.add(workingDirectoryField = new JTextField(configuration.getWorkingDirectory().getAbsolutePath()), cc.xy(3, 3));
		builder.addLabel(I18n.getMessage("workingDirectory"), cc.xy(1, 3)).setLabelFor(workingDirectoryField);
		builder.add(workingDirectoryButton = new JButton(SwingUtils.getIcon("document-open-folder", 16)), cc.xy(5, 3));
		
		workingDirectoryButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				String selectedPath = SwingUtils.showDirectorySelectionDialog(DirectoriesSettingsPanel.this, workingDirectoryField.getText());
				
				if(selectedPath != null)
					workingDirectoryField.setText(selectedPath);
			}
			
		});


		builder.addSeparator(I18n.getMessage("archiveSeparator"), cc.xyw(1, 5, 3));

		builder.add(archiveDirectoryField = new JTextField(configuration.getArchiveDirectory().getAbsolutePath()), cc.xy(3, 7));
		builder.addLabel(I18n.getMessage("archivePath"), cc.xy(1, 7)).setLabelFor(archiveDirectoryField);
		builder.add(archiveDirectoryButton = new JButton(SwingUtils.getIcon("document-open-folder", 16)), cc.xy(5, 7));
		
		archiveDirectoryButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				String selectedPath = SwingUtils.showDirectorySelectionDialog(DirectoriesSettingsPanel.this, archiveDirectoryField.getText());
				
				if(selectedPath != null)
					archiveDirectoryField.setText(selectedPath);
			}
		});

		add(builder.getPanel(), BorderLayout.CENTER);
	}

	@Override
	public void applyConfiguration() {

		configuration.setWorkingDirectory(new File(workingDirectoryField.getText()));
		configuration.setArchiveDirectory(new File(archiveDirectoryField.getText()));
	}

	@Override
	public JPanel getPanel() {

		return this;
	}

}
