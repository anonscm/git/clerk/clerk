/**
 * 
 */
package org.evolvis.clerk.swing.panels;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.evolvis.clerk.core.beans.Addressee;
import org.evolvis.clerk.core.beans.Letter;
import org.evolvis.clerk.core.beans.LetterTemplate;
import org.evolvis.clerk.core.beans.SenderIdentity;
import org.evolvis.clerk.core.config.ClerkConfiguration;
import org.evolvis.clerk.core.config.ClerkConfigurationListener;
import org.evolvis.clerk.core.engine.impl.ClerkWorkflow;
import org.evolvis.clerk.core.engine.impl.DocumentViewer;
import org.evolvis.clerk.swing.panels.settings.SettingsPanel;
import org.evolvis.clerk.swing.utils.I18n;
import org.evolvis.clerk.swing.utils.SwingUtils;
import org.evolvis.clerk.widgets.LetterTemplateSelector;
import org.evolvis.clerk.widgets.SenderIdentitySelector;

import com.jgoodies.forms.builder.ButtonBarBuilder2;
import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class CreateLetterPanel extends JPanel implements ClerkConfigurationListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7105783949318025592L;

	LetterTemplateSelector templateField;
	
	SenderIdentitySelector senderField;

	AddressPanel addresseeField;

	JTextField subjectField;
	JTextArea textArea;
	JTextField openingField;
	JTextField closingField;

	JScrollPane textScrollPane;

	JButton showLetterButton;
	JButton settingsButton;
	JButton printLetterButton;
	
	Frame parent;
	
	ClerkConfiguration configuration;

	public CreateLetterPanel(Frame parent, ClerkConfiguration configuration) {
		
		this.parent = parent;
		this.configuration = configuration;
		
		configuration.addConfigurationListener(this);

		setLayout(new BorderLayout());

		FormLayout layout = new FormLayout(
				"40dlu, 3dlu, pref, 3dlu, pref, 3dlu, 0dlu:grow, 3dlu, pref, 3dlu, pref", // columns
		"pref, 3dlu, pref, 10dlu, pref, 3dlu, pref, 10dlu, pref, 3dlu, pref, 10dlu, pref, 3dlu, pref, 3dlu, pref, 3dlu, pref:grow, 3dlu, pref, 15dlu, pref"); // rows

		PanelBuilder builder = new PanelBuilder(layout);
		builder.setDefaultDialogBorder();

		CellConstraints cc = new CellConstraints();

		builder.addSeparator(I18n.getMessage("separatorSender"), cc.xyw(1, 1, 11));

		builder.add(senderField = new SenderIdentitySelector(configuration.getSenderIdentities()), cc.xyw(3, 3, 9));
		builder.addLabel(I18n.getMessage("senderIdentity"), cc.xy(1, 3)).setLabelFor(senderField);

		
		builder.addSeparator(I18n.getMessage("separatorTemplate"), cc.xyw(1, 5, 11));
		
		builder.add(templateField = new LetterTemplateSelector(configuration.getLetterTemplates()), cc.xyw(3, 7, 9));
		builder.addLabel(I18n.getMessage("template"), cc.xy(1, 7)).setLabelFor(templateField);
		
		builder.addSeparator(I18n.getMessage("separatorAddressee"), cc.xyw(1, 9, 11));

		builder.add(addresseeField = new AddressPanel(), cc.xyw(1, 11, 11));

		builder.addSeparator(I18n.getMessage("separatorLetter"), cc.xyw(1, 13, 11));

		builder.add(subjectField = new JTextField(), cc.xyw(3, 15, 9));
		builder.addLabel(I18n.getMessage("letterSubject"), cc.xy(1, 15)).setLabelFor(subjectField);
		
		builder.add(openingField = new JTextField(), cc.xyw(3, 17, 9));
		builder.addLabel(I18n.getMessage("letterOpening"), cc.xy(1, 17)).setLabelFor(openingField);

		builder.add(textScrollPane = new JScrollPane(textArea = new JTextArea(),
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED),
				cc.xyw(1, 19, 11, CellConstraints.FILL, CellConstraints.FILL));
		textScrollPane.setPreferredSize(new Dimension(450, 120));
		
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		
		SwingUtils.makeTextAreaTabFocusTraversal(textArea);
		
		builder.add(closingField = new JTextField(), cc.xyw(3, 21, 9));
		builder.addLabel(I18n.getMessage("letterClosing"), cc.xy(1, 21)).setLabelFor(closingField);

		ButtonBarBuilder2 buttonBarBuilder = new ButtonBarBuilder2();
		buttonBarBuilder.addButton(settingsButton = new JButton(I18n.getMessage("buttonSettings"), SwingUtils.getIcon("preferences-other")));
		buttonBarBuilder.addGlue();
//		buttonBarBuilder.addButton(printLetterButton = new JButton(I18n.getMessage("buttonPrintLetter"), SwingUtils.getIcon("printer")));
//		buttonBarBuilder.addRelatedGap();
		buttonBarBuilder.addButton(showLetterButton = new JButton(I18n.getMessage("buttonShowLetter"), SwingUtils.getIcon("accessories-text-editor")));
		builder.add(buttonBarBuilder.getPanel(), cc.xyw(1, 23, 11));
		
		settingsButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				JDialog dialog = new JDialog(CreateLetterPanel.this.parent, I18n.getMessage("settingsWindowTitle"), true);
				
				dialog.getContentPane().add(new SettingsPanel(dialog, CreateLetterPanel.this.configuration));
				
				dialog.pack();
				
				dialog.setLocationRelativeTo(CreateLetterPanel.this.parent);
				
				dialog.setVisible(true);
			}
		});

		showLetterButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				final File targetFile = runClerkWorkflow();
				
				if(targetFile == null)
					return;
				
				new Thread() {

					@Override
					public void run() {
						
						new DocumentViewer(CreateLetterPanel.this.configuration.getDocumentViewerCommand()).showDocument(targetFile);
					}
					
				}.start();
			}
		});

		add(builder.getPanel(), BorderLayout.CENTER);
	}
	
	public SenderIdentity getSender() {
		
		return senderField.getSelectedEntity();
	}
	
	public Addressee getAddressee() {
		
		return new Addressee(addresseeField.getAddress());
	}
	
	public LetterTemplate getTemplate() {
		
//		return new LetterTemplate("Brief", new File("/home/fkoest/Dokumente/Privat/Korrespondenz/Vorlagen/Brief.tex"));
		return templateField.getSelectedEntity();
	}
	
	public Letter getLetter() {
		
		return new Letter(
				subjectField.getText(),
				textArea.getText(),
				openingField.getText(),
				closingField.getText());
	}
	
	private File runClerkWorkflow() {
		
		try {
			
			ClerkWorkflow workflow = new ClerkWorkflow(
					configuration,
					getTemplate(),
					getAddressee(),
					getSender(),
					getLetter());
			
			workflow.run();
			
			return workflow.getTargetFile();
			
		} catch(Exception excp) {

			excp.printStackTrace();
			SwingUtils.showErrorDialog(parent, excp);
			
		}
		
		return null;
	}

	@Override
	public void configurationChanged() {
		
		senderField.setEntities(configuration.getSenderIdentities());
		templateField.setEntities(configuration.getLetterTemplates());
	}
}
