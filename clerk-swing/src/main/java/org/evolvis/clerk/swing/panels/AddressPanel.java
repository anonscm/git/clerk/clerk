/**
 * 
 */
package org.evolvis.clerk.swing.panels;

import java.awt.BorderLayout;
import java.util.Locale;

import javax.swing.JFormattedTextField;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.evolvis.clerk.core.beans.Address;
import org.evolvis.clerk.core.beans.AddressImpl;
import org.evolvis.clerk.core.utils.LaTeXHelper;
import org.evolvis.clerk.core.utils.PostalCodeHelper;
import org.evolvis.clerk.swing.utils.I18n;
import org.evolvis.clerk.swing.utils.SwingUtils;
import org.evolvis.clerk.widgets.CountrySelector;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class AddressPanel extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3048793548263664766L;
	JTextField nameField;
	JTextField nameAdditionField;
	JTextField streetNameField;
	JTextField streetNumberField;
	JTextField postalCodeField;
	JTextField townField;
	CountrySelector countryField;

	public AddressPanel() {
		
		setLayout(new BorderLayout());

		FormLayout layout = new FormLayout(
				"40dlu, 3dlu, pref, 3dlu, pref, 3dlu, 0dlu:grow, 3dlu, pref, 3dlu, pref", // columns
		"pref, 3dlu, pref, 3dlu, pref, 3dlu, pref, 3dlu, pref"); // rows

		PanelBuilder builder = new PanelBuilder(layout);
		
		CellConstraints cc = new CellConstraints();
		
		builder.add(countryField = new CountrySelector(), cc.xyw(3, 1, 9));
		builder.addLabel(I18n.getMessage("addressCountry"), cc.xy(1, 1)).setLabelFor(countryField);
		
		builder.add(nameField = new JTextField(), cc.xyw(3, 3, 9));
		builder.addLabel(I18n.getMessage("addressName"), cc.xy(1, 3)).setLabelFor(nameField);
		
		builder.add(nameAdditionField = new JTextField(), cc.xyw(3, 5, 9));
		builder.addLabel(I18n.getMessage("addressNameAddition"), cc.xy(1, 5)).setLabelFor(nameAdditionField);

		builder.add(streetNameField = new JTextField(), cc.xyw(3, 7, 5));
		builder.addLabel(I18n.getMessage("addressStreetName"), cc.xy(1, 7)).setLabelFor(streetNameField);

		builder.add(streetNumberField = new JTextField(4), cc.xy(11, 7));
		builder.addLabel(I18n.getMessage("addressStreetNumber"), cc.xy(9, 7)).setLabelFor(streetNumberField);

		// TODO get local from addressee
		builder.add(postalCodeField = new JFormattedTextField(SwingUtils.getPostalCodeFormatter(Locale.getDefault())), cc.xy(3, 9));
		builder.addLabel(I18n.getMessage("addressPostalCode"), cc.xy(1, 9)).setLabelFor(postalCodeField);
		postalCodeField.setColumns(5);
		postalCodeField.getDocument().addDocumentListener(new DocumentListener() {
			
			public void autofillTown() {
				
				if(postalCodeField.getText().trim().length() != 5)
					return;
				
				String town = PostalCodeHelper.getTownForPostalCode(Integer.valueOf(postalCodeField.getText()), countryField.getSelectedCountry());
				
				if(town != null)
					townField.setText(town);
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				
				autofillTown();
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				
				autofillTown();
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				
				autofillTown();
			}
		});
		builder.add(townField = new JTextField(), cc.xyw(7, 9, 5));
		builder.addLabel(I18n.getMessage("addressTown"), cc.xy(5, 9)).setLabelFor(townField);
		
		
		add(builder.getPanel(), BorderLayout.CENTER);
	}
	
	public void setAddress(Address address) {
		
		nameField.setText(address.getAddressName());
		nameAdditionField.setText(address.getNameAddition());
		streetNameField.setText(address.getStreetName());
		streetNumberField.setText(address.getStreetNumber());
		postalCodeField.setText(PostalCodeHelper.getPostalCodeForInt(address.getPostalCode(), address.getCountry()));
		townField.setText(address.getTown());
		countryField.setSelectedItem(address.getCountry());
	}
	
	public Address getAddress() {
		
		return new AddressImpl(
				LaTeXHelper.escapeLaTeXCharacters(nameField.getText()),
				LaTeXHelper.escapeLaTeXCharacters(nameAdditionField.getText()),
				LaTeXHelper.escapeLaTeXCharacters(streetNameField.getText()),
				LaTeXHelper.escapeLaTeXCharacters(streetNumberField.getText()),
				Integer.valueOf(postalCodeField.getText()),
				LaTeXHelper.escapeLaTeXCharacters(townField.getText()),
				countryField.getSelectedCountry());
	}
	
	public void clear() {
		
		nameField.setText("");
		nameAdditionField.setText("");
		streetNameField.setText("");
		streetNumberField.setText("");
		postalCodeField.setText("");
		townField.setText("");
		countryField.setSelectedItem(Locale.getDefault());
	}
}
