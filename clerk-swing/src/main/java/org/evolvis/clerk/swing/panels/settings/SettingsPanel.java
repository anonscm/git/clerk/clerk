/**
 * 
 */
package org.evolvis.clerk.swing.panels.settings;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.evolvis.clerk.core.config.ClerkConfiguration;
import org.evolvis.clerk.swing.utils.I18n;
import org.evolvis.clerk.swing.utils.SwingUtils;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.factories.ButtonBarFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class SettingsPanel extends JPanel implements SettingsModule {

	JButton submitButton;
	JButton cancelButton;

	SettingsModule identitiesManagement;
	SettingsModule generalSettings;
	SettingsModule templatesManagement;
	SettingsModule commandsSettings;
	SettingsModule directoriesSettings;

	Container placeholder;

	ClerkConfiguration configuration;

	JDialog parent;

	private final static String IDENTITIES = "identities:preferences-desktop-user";
	private final static String TEMPLATES = "templates:accessories-text-editor";
	private final static String DIRECTORIES = "directories:system-file-manager";
	private final static String COMMANDS = "commands:applications-system";

	int menuIndexHover;
	Color hoverColor;

	public SettingsPanel(final JDialog parent, ClerkConfiguration configuration)  {

		this.configuration = configuration;
		this.parent = parent;

		setLayout(new BorderLayout());
		setPreferredSize(new Dimension(650, 400));

		FormLayout layout = new FormLayout("max(pref;40dlu), 5dlu, pref:grow", // columns
		"pref:grow, 10dlu, pref"); // rows

		PanelBuilder builder = new PanelBuilder(layout);
		builder.setDefaultDialogBorder();

		CellConstraints cc = new CellConstraints();

		final JList menu = new JList(new String[] { IDENTITIES, TEMPLATES, DIRECTORIES, COMMANDS});
		menu.setBorder(BorderFactory.createLineBorder(UIManager.getColor("Tree.selectionBorderColor")));
		menu.setCellRenderer(new MenuCellRenderer());
		menu.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		menu.setSelectedIndex(0);
		
		HoverListener hoverListener = new HoverListener(menu);
		menu.addMouseListener(hoverListener);
		menu.addMouseMotionListener(hoverListener);
		hoverColor = new Color(
				UIManager.getColor("List.selectionBackground").getRed()+20,
				UIManager.getColor("List.selectionBackground").getGreen()+20,
				UIManager.getColor("List.selectionBackground").getBlue()+20);;


		menu.addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {

				if(e.getValueIsAdjusting())
					return;

				placeholder.removeAll();

				if(menu.getSelectedValue().equals(IDENTITIES))
					placeholder.add(getIdentitiesManagement().getPanel(), BorderLayout.CENTER);	
				else if(menu.getSelectedValue().equals(TEMPLATES))
					placeholder.add(getTemplatesManagement().getPanel(), BorderLayout.CENTER);
				else if(menu.getSelectedValue().equals(DIRECTORIES))
					placeholder.add(getDirectoriesSettings().getPanel(), BorderLayout.CENTER);
				else if(menu.getSelectedValue().equals(COMMANDS))
					placeholder.add(getCommandsSettings().getPanel(), BorderLayout.CENTER);

				placeholder.repaint();
				placeholder.validate();
			}
		});

		builder.add(menu, cc.xy(1, 1, CellConstraints.DEFAULT, CellConstraints.FILL));

		placeholder = new JPanel(new BorderLayout());
		placeholder.add(getIdentitiesManagement().getPanel());

		builder.add(placeholder, cc.xy(3, 1, CellConstraints.FILL, CellConstraints.FILL));

		builder.add(ButtonBarFactory.buildOKCancelBar(
				submitButton = new JButton(I18n.getMessage("buttonSubmit"), SwingUtils.getIcon("dialog-ok")),
				cancelButton = new JButton(I18n.getMessage("buttonCancel"), SwingUtils.getIcon("dialog-cancel"))), cc.xyw(1, 3, 3));

		cancelButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				parent.setVisible(false);
			}

		});

		submitButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				applyConfiguration();
				SettingsPanel.this.configuration.save();
				parent.setVisible(false);
			}

		});

		parent.getRootPane().setDefaultButton(cancelButton);

		add(builder.getPanel(), BorderLayout.CENTER);
	}

	private SettingsModule getIdentitiesManagement() {

		if(identitiesManagement == null)
			identitiesManagement = new SenderIdentitiesManagementPanel(configuration, parent);

		return identitiesManagement;
	}

	private SettingsModule getTemplatesManagement() {

		if(templatesManagement == null)
			templatesManagement = new TemplatesManagementPanel(configuration, parent);

		return templatesManagement;
	}

	private SettingsModule getDirectoriesSettings() {

		if(directoriesSettings == null)
			directoriesSettings = new DirectoriesSettingsPanel(configuration);

		return directoriesSettings;
	}

	private SettingsModule getCommandsSettings() {

		if(commandsSettings == null)
			commandsSettings = new CommandSettingsPanel(configuration);

		return commandsSettings;
	}

	private class MenuCellRenderer extends JLabel implements ListCellRenderer {

		public MenuCellRenderer( ) { 

			setHorizontalAlignment(JLabel.CENTER);
			setVerticalAlignment(JLabel.CENTER);
			setHorizontalTextPosition(JLabel.CENTER);
			setVerticalTextPosition(JLabel.BOTTOM);
			setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
			setOpaque(true);
		} 

		public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean hasFocus) { 

			String[] data = value.toString().split(":"); 

			setText(I18n.getMessage(data[0]));
			setIcon(SwingUtils.getIcon(data[1], 32));
			setBackground(isSelected ? UIManager.getColor("List.selectionBackground") : (index == menuIndexHover ? hoverColor : UIManager.getColor("List.background")));
			setForeground(isSelected ? UIManager.getColor("List.selectionForeground") : UIManager.getColor("List.foreground"));
			setBorder(isSelected ? BorderFactory.createBevelBorder(BevelBorder.RAISED) : BorderFactory.createEmptyBorder(10, 10, 10, 10));

			return this;
		}
	}

	@Override
	public void applyConfiguration() {

		getCommandsSettings().applyConfiguration();
		getTemplatesManagement().applyConfiguration();
		getIdentitiesManagement().applyConfiguration();
		getDirectoriesSettings().applyConfiguration();
	}

	@Override
	public JPanel getPanel() {

		return this;
	}

	private class HoverListener extends MouseAdapter {
		
		JList menu;
		boolean exited;
		
		public HoverListener(JList menu) {
			
			this.menu = menu;
			exited = true;
		}

		private void updateHoverCell(Point currentMousePosition) {

			int indexHover = menu.locationToIndex(currentMousePosition);

			if(menuIndexHover == indexHover)
				return;

			menuIndexHover = indexHover;

			menu.repaint();
		}

		@Override
		public void mouseMoved(MouseEvent e) {

			if(!exited)
				updateHoverCell(e.getPoint());
		}

		@Override
		public void mouseEntered(MouseEvent e) {

			exited = false;
			updateHoverCell(e.getPoint());
		}

		@Override
		public void mouseExited(MouseEvent e) {

			exited = true;
			menuIndexHover = -1;
			menu.repaint();
		}

	}
}