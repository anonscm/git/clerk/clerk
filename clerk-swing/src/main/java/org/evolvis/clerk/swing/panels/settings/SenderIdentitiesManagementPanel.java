/**
 * 
 */
package org.evolvis.clerk.swing.panels.settings;

import javax.swing.JDialog;
import javax.swing.JPanel;

import org.evolvis.clerk.core.beans.SenderIdentity;
import org.evolvis.clerk.core.config.ClerkConfiguration;
import org.evolvis.clerk.swing.utils.I18n;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class SenderIdentitiesManagementPanel extends AbstractEntityManagementPanel<SenderIdentity> implements SettingsModule {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3351375107462383889L;
	
	ClerkConfiguration configuration;

	public SenderIdentitiesManagementPanel(ClerkConfiguration configuration, JDialog parent) {
		
		super(parent, new SenderIdentityEditPanel(), I18n.getMessage("createIdentity"), I18n.getMessage("editIdentity"), I18n.getMessage("deleteIdentity"));
		this.configuration = configuration;
		
		setEntities(configuration.getSenderIdentities());
	}

	@Override
	public void applyConfiguration() {
		
		configuration.setSenderIdentities(getEntities());
	}

	@Override
	public JPanel getPanel() {
		
		return this;
	}
}
