/**
 * 
 */
package org.evolvis.clerk.swing.panels.settings;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JTextField;

import org.evolvis.clerk.core.config.ClerkConfiguration;
import org.evolvis.clerk.swing.utils.I18n;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class CommandSettingsPanel extends JPanel implements SettingsModule {

	JTextField latexCompilerCommand;
	JTextField documentViewerCommand;
	
	ClerkConfiguration configuration;

	public CommandSettingsPanel(ClerkConfiguration configuration) {

		this.configuration = configuration;
		
		setLayout(new BorderLayout());

		FormLayout layout = new FormLayout(
				"pref, 3dlu, pref:grow", // columns
		"pref, 3dlu, pref, 10dlu, pref, 3dlu, pref"); // rows

		PanelBuilder builder = new PanelBuilder(layout);
		CellConstraints cc = new CellConstraints();
		
		builder.addSeparator(I18n.getMessage("latexCommands"), cc.xyw(1, 1, 3));
		
		builder.add(latexCompilerCommand = new JTextField(configuration.getLatexToTargetCommand()), cc.xy(3, 3));
		builder.addLabel(I18n.getMessage("latexCompilerCommand"), cc.xy(1, 3)).setLabelFor(latexCompilerCommand);
		
		
		builder.addSeparator(I18n.getMessage("documentViewerCommands"), cc.xyw(1, 5, 3));
		
		builder.add(documentViewerCommand = new JTextField(configuration.getDocumentViewerCommand()), cc.xy(3, 7));
		builder.addLabel(I18n.getMessage("documentViewerCommand"), cc.xy(1, 7)).setLabelFor(documentViewerCommand);
		
		add(builder.getPanel(), BorderLayout.CENTER);
	}

	@Override
	public void applyConfiguration() {
		
		configuration.setLatexToTargetCommand(latexCompilerCommand.getText());
		configuration.setTargetViewerCommand(documentViewerCommand.getText());
	}

	@Override
	public JPanel getPanel() {
		
		return this;
	}
}
