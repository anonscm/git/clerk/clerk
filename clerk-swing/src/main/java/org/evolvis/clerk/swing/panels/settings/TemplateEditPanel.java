/**
 * 
 */
package org.evolvis.clerk.swing.panels.settings;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.evolvis.clerk.core.beans.LetterTemplate;
import org.evolvis.clerk.swing.panels.AbstractEntityEditPanel;
import org.evolvis.clerk.swing.utils.I18n;
import org.evolvis.clerk.swing.utils.SwingUtils;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.factories.ButtonBarFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class TemplateEditPanel extends AbstractEntityEditPanel<LetterTemplate> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6579188383100025028L;
	
	private JButton submitButton;
	private JButton cancelButton;
	
	private Action submitAction;
	private Action cancelAction;
	
	private JTextField templateNameField;
	private JTextField templatePathField;
	private JButton openFileChooserButton;
	private TemplateRessourcesPanel templateRessourcesPanel;

	public TemplateEditPanel(JDialog parent) {
		
		setLayout(new BorderLayout());

		FormLayout layout = new FormLayout(
				"max(pref;40dlu), 3dlu, max(pref;150dlu):grow, 1dlu, pref", // columns
		"pref, 10dlu, pref, 3dlu, pref, 10dlu, pref, 3dlu, pref, 15dlu, pref"); // rows
		
		PanelBuilder builder = new PanelBuilder(layout);
		builder.setDefaultDialogBorder();
		CellConstraints cc = new CellConstraints();
		
		builder.add(templateNameField = new JTextField(), cc.xyw(3, 1, 3));
		builder.addLabel(I18n.getMessage("templateName"), cc.xy(1, 1)).setLabelFor(templateNameField);
		
		builder.addSeparator(I18n.getMessage("templateProperties"), cc.xyw(1, 3, 5));
		
		builder.add(templatePathField = new JTextField(), cc.xy(3, 5));
		builder.addLabel(I18n.getMessage("templatePath"), cc.xy(1, 5)).setLabelFor(templatePathField);
		
		builder.add(openFileChooserButton = new JButton(SwingUtils.getIcon("document-open-folder", 16)), cc.xy(5, 5));
		
		
		builder.addSeparator(I18n.getMessage("templateRessources"), cc.xyw(1, 7, 5));
		
		builder.add(templateRessourcesPanel = new TemplateRessourcesPanel(parent), cc.xyw(1, 9, 5));
		
		openFileChooserButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				String result = SwingUtils.showFileSelectionDialog(TemplateEditPanel.this,
						templatePathField.getText(),
						new FileNameExtensionFilter(I18n.getMessage("fileFilterLaTeX"), "tex"));
				
				if(result != null)
					templatePathField.setText(result);
			}
		});
		
		builder.add(ButtonBarFactory.buildOKCancelBar(
				submitButton = new JButton("", SwingUtils.getIcon("dialog-ok")),
				cancelButton = new JButton(I18n.getMessage("buttonCancel"), SwingUtils.getIcon("dialog-cancel"))), cc.xyw(1, 11, 5));
		
		add(builder.getPanel(), BorderLayout.CENTER);
		
		submitButton.addActionListener(new ActionListener() {

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if(submitAction != null)
					submitAction.actionPerformed(e);
				
			}
		});
		
		cancelButton.addActionListener(new ActionListener() {

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if(cancelAction != null)
					cancelAction.actionPerformed(e);
			}
		});
	}

	@Override
	public void setActionName(String actionName) {
		
		submitButton.setText(actionName);
	}

	@Override
	public void setCancelAction(Action cancelAction) {
		
		this.cancelAction = cancelAction;
	}

	@Override
	public void setSubmitAction(Action submitAction) {

		this.submitAction = submitAction;
	}

	/**
	 * @see org.evolvis.clerk.swing.panels.AbstractEntityEditPanel#setEntity(java.lang.Object)
	 */
	@Override
	public void setEntity(LetterTemplate template) {
		
		templateNameField.setText(template.getTemplateName());
		
		if(template.getFile() != null)
			templatePathField.setText(template.getFile().getAbsolutePath());
		
		if(template.getRessources() != null)
			templateRessourcesPanel.setEntities(template.getRessources());
	}

	/**
	 * @see org.evolvis.clerk.swing.panels.AbstractEntityEditPanel#clear()
	 */
	@Override
	public void clear() {
		
		templateNameField.setText("");
		templatePathField.setText("");
	}

	@Override
	public LetterTemplate getEntity() {
		
		// TODO check validity of template path
		return new LetterTemplate(templateNameField.getText(), new File(templatePathField.getText()), templateRessourcesPanel.getEntities());
	}
}
