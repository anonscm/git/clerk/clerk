/**
 * 
 */
package org.evolvis.clerk.swing.panels.settings;

import javax.swing.JPanel;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public interface SettingsModule {

	public void applyConfiguration();
	
	public JPanel getPanel();
}
