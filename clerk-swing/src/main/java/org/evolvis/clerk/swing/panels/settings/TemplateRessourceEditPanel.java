/**
 * 
 */
package org.evolvis.clerk.swing.panels.settings;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JTextField;

import org.evolvis.clerk.core.beans.LetterTemplateRessource;
import org.evolvis.clerk.swing.panels.AbstractEntityEditPanel;
import org.evolvis.clerk.swing.utils.I18n;
import org.evolvis.clerk.swing.utils.SwingUtils;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.factories.ButtonBarFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class TemplateRessourceEditPanel  extends AbstractEntityEditPanel<LetterTemplateRessource> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1831078844615273020L;
	
	private Action submitAction;
	private Action cancelAction;
	
	private JTextField filePathField;
	private JButton filePathButton;
	private JButton submitButton;
	private JButton cancelButton;
	
	private String lastFilePath;
	
	public TemplateRessourceEditPanel() {
		
		setLayout(new BorderLayout());

		FormLayout layout = new FormLayout(
				"pref, 3dlu, max(pref;150dlu):grow, 3dlu, pref", // columns
		"pref, 15dlu, pref"); // rows

		PanelBuilder builder = new PanelBuilder(layout);
		builder.setDefaultDialogBorder();
		CellConstraints cc = new CellConstraints();
		
		builder.add(filePathField = new JTextField(), cc.xy(3, 1));
		builder.addLabel(I18n.getMessage("templateRessourcePath"), cc.xy(1, 1)).setLabelFor(filePathField);
		builder.add(filePathButton = new JButton(SwingUtils.getIcon("document-open-folder", 16)), cc.xy(5, 1));
		
		filePathButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				String selectedPath = SwingUtils.showFileSelectionDialog(TemplateRessourceEditPanel.this, lastFilePath, null);
				
				if(selectedPath != null)
					filePathField.setText(selectedPath);
				
				lastFilePath = selectedPath;
			}
			
		});
		
		builder.add(ButtonBarFactory.buildOKCancelBar(
				submitButton = new JButton("", SwingUtils.getIcon("dialog-ok")),
				cancelButton = new JButton(I18n.getMessage("buttonCancel"), SwingUtils.getIcon("dialog-cancel"))), cc.xyw(1, 3, 5));
		
		add(builder.getPanel(), BorderLayout.CENTER);
		
		submitButton.addActionListener(new ActionListener() {

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if(submitAction != null)
					submitAction.actionPerformed(e);
				
			}
		});
		
		cancelButton.addActionListener(new ActionListener() {

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if(cancelAction != null)
					cancelAction.actionPerformed(e);
			}
		});
	}

	@Override
	public void clear() {
		
		filePathField.setText("");
	}

	@Override
	public LetterTemplateRessource getEntity() {

		// TODO check field value for validity
		return new LetterTemplateRessource(new File(filePathField.getText()));
	}

	@Override
	public void setActionName(String actionName) {
		
		submitButton.setText(actionName);
	}

	@Override
	public void setCancelAction(Action cancelAction) {
		
		this.cancelAction = cancelAction;
	}

	@Override
	public void setEntity(LetterTemplateRessource entity) {
		
		filePathField.setText(entity.getRessourceFile().getAbsolutePath());
	}

	@Override
	public void setSubmitAction(Action submitAction) {
		
		this.submitAction = submitAction;
	}

}
