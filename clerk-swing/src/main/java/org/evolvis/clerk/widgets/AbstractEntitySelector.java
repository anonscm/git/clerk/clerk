/**
 * 
 */
package org.evolvis.clerk.widgets;

import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

import org.evolvis.clerk.core.beans.AbstractClerkBean;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public abstract class AbstractEntitySelector<T extends AbstractClerkBean> extends JComboBox {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5344550006171542041L;

	public AbstractEntitySelector(List<T> entities) {
		
		super(new AbstractEntityModel<T>(entities));
		
		if(!entities.isEmpty())
			setSelectedIndex(0);
	}
	
	@SuppressWarnings("unchecked")
	public T getSelectedEntity() {
		
		return (T)getSelectedItem();
	}
	
	public void setEntities(List<T> entities) {
		
		((AbstractEntityModel<T>)getModel()).setEntities(entities);
	}
	
	private static class AbstractEntityModel<T extends AbstractClerkBean> extends DefaultComboBoxModel {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = -714194186218849549L;

		public AbstractEntityModel(List<T> entities) {
			
			setEntities(entities);
		}
		
		public void setEntities(List<T> entities) {
			
			removeAllElements();
			for(T entity: entities)
				addElement(entity);
		}
	}
}
