/**
 * 
 */
package org.evolvis.clerk.widgets;

import java.util.List;

import org.evolvis.clerk.core.beans.LetterTemplate;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class LetterTemplateSelector extends AbstractEntitySelector<LetterTemplate> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -89677897456421546L;

	public LetterTemplateSelector(List<LetterTemplate> entities) {
		super(entities);
	}	
}
