/**
 * 
 */
package org.evolvis.clerk.widgets;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JList;

import org.evolvis.clerk.core.beans.Country;
import org.evolvis.clerk.core.utils.CountryHelper;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class CountrySelector extends JComboBox {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3558580717168495538L;

	public CountrySelector() {
		
		super(CountryHelper.getAllCountriesSorted().toArray());
		
		this.setRenderer(new DefaultListCellRenderer() {

			/**
			 * 
			 */
			private static final long serialVersionUID = -4622466149202250884L;

			@Override
			public Component getListCellRendererComponent(JList list,
                    Object value,
                    int index,
                    boolean isSelected,
                    boolean cellHasFocus) {
				
				Country country = (Country) value;
				
				value = country.getDisplayName();

				Component comp = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);		
				
				return comp;
			}
		});
		
		// Select system locale by default
		this.setSelectedItem(Country.getSystemCountry());
	}
	
	public Country getSelectedCountry() {
		
		return (Country)getSelectedItem();
	}
}
