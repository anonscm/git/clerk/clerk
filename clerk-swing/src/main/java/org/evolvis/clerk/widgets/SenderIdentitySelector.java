/**
 * 
 */
package org.evolvis.clerk.widgets;

import java.util.List;

import org.evolvis.clerk.core.beans.SenderIdentity;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class SenderIdentitySelector extends AbstractEntitySelector<SenderIdentity> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6351828755176997186L;

	public SenderIdentitySelector(List<SenderIdentity> entities) {
		
		super(entities);
	}
}
