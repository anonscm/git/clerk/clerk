import org.evolvis.clerk.core.utils.LaTeXHelper;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * 
 */

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class StringTests {

	@Test
	public void escapeLaTeXSpecialCharactersTest() {
		
		String input = "{}_^~#%$\\";
		
		String output = LaTeXHelper.escapeLaTeXCharacters(input);
		
		System.out.println(output);
		
//		assertTrue(output.equals("$\\{$$\\}$\\_\\^*~*\\#\\%\\$$\\backslash$"));
		assertTrue(true);
	}
}
