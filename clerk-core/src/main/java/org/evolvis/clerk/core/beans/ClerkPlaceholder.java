/**
 * 
 */
package org.evolvis.clerk.core.beans;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 
 * This annotation marks fields which also describe placeholder-values for clerk templates
 * 
 * Clerk accesses this annoation at runtime via reflection, so the RetentionPolicy is set to RUNTIME.
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface ClerkPlaceholder {

	String value();
}
