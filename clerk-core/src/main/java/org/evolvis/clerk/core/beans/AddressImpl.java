/**
 * 
 */
package org.evolvis.clerk.core.beans;


/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class AddressImpl implements Address {
	
	String addressName;
	String nameAddition;
	String streetName;
	String streetNumber;
	int postalCode;
	String town;
	Country country;
	
	public AddressImpl(String addressName, String nameAddition, String streetName, String streetNumber, int postalCode, String town, Country country) {
		
		this.addressName = addressName;
		this.nameAddition = nameAddition;
		this.streetName = streetName;
		this.streetNumber = streetNumber;
		this.postalCode = postalCode;
		this.town = town;
		this.country = country;
	}

	/**
	 * @see org.evolvis.clerk.core.beans.Address#getCountry()
	 */
	@Override
	public Country getCountry() {
		
		return country;
	}

	/**
	 * @see org.evolvis.clerk.core.beans.Address#getAddressName()
	 */
	@Override
	public String getAddressName() {
		
		return addressName;
	}

	/**
	 * @see org.evolvis.clerk.core.beans.Address#getPostalCode()
	 */
	@Override
	public int getPostalCode() {
		
		return postalCode;
	}

	/**
	 * @see org.evolvis.clerk.core.beans.Address#getStreetName()
	 */
	@Override
	public String getStreetName() {
		
		return streetName;
	}

	/**
	 * @see org.evolvis.clerk.core.beans.Address#getStreetNumber()
	 */
	@Override
	public String getStreetNumber() {
		
		return streetNumber;
	}

	/**
	 * @see org.evolvis.clerk.core.beans.Address#getTown()
	 */
	@Override
	public String getTown() {
		
		return town;
	}

	@Override
	public String getNameAddition() {
		
		return nameAddition;
	}

}
