/**
 * 
 */
package org.evolvis.clerk.core.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import org.evolvis.clerk.core.ClerkLogger;
import org.evolvis.clerk.core.beans.Country;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class PostalCodeHelper {

	public static String getPostalCodeForInt(int postalCode, Country country) {
		
		// If no country is given, return null;
		if(country == null)
			return null;
		
		if(country.getIsoCode().equals("DE")) {
			
			// german postal codes have 5 digits and may have a leading zero
			// so prepend zero to a 4 digit integer 
			
			if(postalCode < 10000 && postalCode > 999)
				return "0"+postalCode;
			else if(postalCode >= 10000 && postalCode < 100000)
				return String.valueOf(postalCode);
			else 
				throw new RuntimeException("Given postal code "+postalCode+" is not a valid German postal code.");
		}	
		
		return String.valueOf(postalCode);
	}
	
	public static String getTownForPostalCode(int postalCode, Country country) {
				
		Properties postalCodes = new Properties();
		
		try {
			
			InputStream inputStream = PostalCodeHelper.class.getResourceAsStream("/org/evolvis/clerk/postalcodes/postal_codes_"+country.getIsoCode()+".properties");
			
			if(inputStream == null)
				return null;
			
			InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
			
			postalCodes.load(inputStreamReader);
			
		} catch (IOException e) {
			
			ClerkLogger.log("Could not get Town for Postal Code "+postalCode, e);
			return null;
		}
		
		return postalCodes.getProperty(getPostalCodeForInt(postalCode, country));
	}
}
