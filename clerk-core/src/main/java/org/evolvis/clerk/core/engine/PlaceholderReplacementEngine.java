/**
 * 
 */
package org.evolvis.clerk.core.engine;

import java.io.File;
import java.util.Map;

import org.evolvis.clerk.core.error.ClerkReplacementException;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public interface PlaceholderReplacementEngine {

	public void replaceTags(File workFile, Map<String, Object> replaceMap, boolean removeRemainingTags) throws ClerkReplacementException;
}
