/**
 * 
 */
package org.evolvis.clerk.core.beans;

import java.io.File;

import org.simpleframework.xml.Attribute;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class LetterTemplateRessource extends AbstractClerkBean {
	
	@Attribute
	private String ressourceFilePath;
	
	
	public LetterTemplateRessource() {
		
	}
	
	public LetterTemplateRessource(File ressourceFile) {
		
		setRessourceFile(ressourceFile);
	}
	
	public void setRessourceFile(File ressourceFile) {
		
		ressourceFilePath = ressourceFile.getAbsolutePath();
	}
	
	public File getRessourceFile() {
		
		return new File(ressourceFilePath);
	}

	/**
	 * @see org.evolvis.clerk.core.beans.AbstractClerkBean#getDisplayName()
	 */
	@Override
	public String getDisplayName() {
		
		return getRessourceFile().getName();
	}

}
