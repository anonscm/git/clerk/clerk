/**
 * 
 */
package org.evolvis.clerk.core.error;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class ClerkCompilerException extends Exception {

	public ClerkCompilerException() {
		
	}
	
	public ClerkCompilerException(String message) {
		
		super(message);
	}
	
	public ClerkCompilerException(Throwable throwable) {
		
		super(throwable);
	}
}
