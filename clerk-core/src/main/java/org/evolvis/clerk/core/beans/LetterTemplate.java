/**
 * 
 */
package org.evolvis.clerk.core.beans;

import java.io.File;
import java.util.List;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class LetterTemplate extends AbstractClerkBean {
	
	@Attribute
	String templateName;
	
	@Element
	String templateFile;

	@ElementList(required=false)
	List<LetterTemplateRessource> templateRessources;
	
	public LetterTemplate() {
		
	}
	
	public LetterTemplate(String name, File file, List<LetterTemplateRessource> ressources) {
		
		this.templateName = name;
		this.templateFile = file.getAbsolutePath();
		this.templateRessources = ressources;
	}
	
	public File getFile() {
		
		if(templateFile != null)
			return new File(templateFile);
		
		return null;
	}
	
	public String getTemplateName() {
		
		return templateName;
	}
	
	public void setRessources(List<LetterTemplateRessource> ressources) {
		
		this.templateRessources = ressources;
	}
	
	public List<LetterTemplateRessource> getRessources() {
		
		return templateRessources;
	}

	@Override
	public String getDisplayName() {
		
		return getTemplateName();
	}	
}
