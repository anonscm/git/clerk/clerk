/**
 * 
 */
package org.evolvis.clerk.core.beans;



/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class Addressee extends AbstractClerkBean implements Address {

	@ClerkPlaceholder("ADDRESSEE-NAME")
	String addresseeName;
	
	@ClerkPlaceholder("ADDRESSEE-NAME-ADDITION")
	String addresseeNameAddition;
	
	@ClerkPlaceholder("ADDRESSEE-STREET-NAME")
	String addresseeStreetName;
	
	@ClerkPlaceholder("ADDRESSEE-STREET-NUMBER")
	String addresseeStreetNumber;
	
	@ClerkPlaceholder("ADDRESSEE-POSTAL-CODE")
	Integer addresseePostalCode;
	
	@ClerkPlaceholder("ADDRESSEE-TOWN")
	String addresseeTown;
	
	@ClerkPlaceholder("ADDRESSEE-COUNTRY")
	Country addresseeCountry;
	
	@ClerkPlaceholder("ADDRESS")
	String completeAddress;
	
	public Addressee() {
		
	}
	
	public Addressee(Address address) {
		
		this(address.getAddressName(), address.getNameAddition(), address.getStreetName(), address.getStreetNumber(), address.getPostalCode(), address.getTown(), address.getCountry());
	}
	
	public Addressee(String addressName, String nameAddition, String streetName, String streetNumber, int postalCode, String town, Country country) {
		
		this.addresseeName = addressName;
		this.addresseeNameAddition = nameAddition;
		this.addresseeStreetName = streetName;
		this.addresseeStreetNumber = streetNumber;
		this.addresseePostalCode = postalCode;
		this.addresseeTown = town;
		this.addresseeCountry = country;
	}
	
	public void generateAddress(boolean addCountry) {
		
		completeAddress = appendLinebreak(addresseeName) + appendLinebreak(addresseeNameAddition) + addresseeStreetName + " " + addresseeStreetNumber + "\\\\\\\\" + addresseePostalCode + " " + addresseeTown + (addCountry ? "\\\\\\\\" + addresseeCountry : "");
	}
	
	private String appendLinebreak(String string) {
		
		if(string == null | string.trim().length() == 0)
			return "";
		
		return string + "\\\\\\\\";
	}

	@Override
	public String getAddressName() {
		
		return addresseeName;
	}

	@Override
	public int getPostalCode() {
		
		return addresseePostalCode;
	}

	@Override
	public String getStreetName() {
		
		return addresseeStreetName;
	}

	@Override
	public String getStreetNumber() {
		
		return addresseeStreetNumber;
	}

	@Override
	public String getTown() {
		
		return addresseeTown;
	}

	@Override
	public Country getCountry() {
		
		return addresseeCountry;
	}

	@Override
	public String getDisplayName() {
		
		return getAddressName();
	}

	@Override
	public String getNameAddition() {
		
		return this.addresseeNameAddition;
	}
}
