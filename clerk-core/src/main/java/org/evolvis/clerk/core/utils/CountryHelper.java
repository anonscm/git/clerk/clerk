/**
 * 
 */
package org.evolvis.clerk.core.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.evolvis.clerk.core.beans.Country;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class CountryHelper {

	public static List<Country> getAllCountriesSorted() {

		List<Country> countries = new ArrayList<Country>();
		
		// This map stores the countries which have already been added 
		Map<String, Boolean> addedCountries = new HashMap<String, Boolean>();

		for(Locale locale : Locale.getAvailableLocales()) {
			if(!locale.getDisplayCountry().trim().isEmpty() && !addedCountries.containsKey(locale.getCountry())) {
				countries.add(new Country(locale));
				addedCountries.put(locale.getCountry(), Boolean.TRUE);
			}
		}
		
		// does not work. why?
		Collections.sort(countries);
		
		return countries;
	}
}
