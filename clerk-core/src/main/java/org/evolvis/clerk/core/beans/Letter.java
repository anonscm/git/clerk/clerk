/**
 * 
 */
package org.evolvis.clerk.core.beans;

import org.simpleframework.xml.Element;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class Letter extends AbstractClerkBean {

	@Element
	@ClerkPlaceholder("LETTER-SUBJECT")
	String letterSubject;
	
	@Element
	@ClerkPlaceholder("LETTER-TEXT")
	String letterText;
	
	@Element
	@ClerkPlaceholder("LETTER-OPENING")
	String letterOpening;
	
	@Element
	@ClerkPlaceholder("LETTER-CLOSING")
	String letterClosing;
	
	public Letter() {
		
	}
	
	public Letter(String subject, String text, String opening, String closing) {
		
		letterSubject = subject;
		letterText = text;
		letterOpening = opening;
		letterClosing = closing;
	}
	
	
	public String getSubject() {
		
		return letterSubject;
	}

	@Override
	public String getDisplayName() {
		
		return getSubject();
	}
}
