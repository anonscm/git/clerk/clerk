/**
 * 
 */
package org.evolvis.clerk.core.engine.impl;

import java.io.File;

import org.evolvis.clerk.core.ClerkLogger;
import org.evolvis.clerk.core.beans.Addressee;
import org.evolvis.clerk.core.beans.Letter;
import org.evolvis.clerk.core.beans.LetterTemplate;
import org.evolvis.clerk.core.beans.SenderIdentity;
import org.evolvis.clerk.core.config.ClerkConfiguration;
import org.evolvis.clerk.core.engine.ClerkCompiler;
import org.evolvis.clerk.core.utils.ReplaceMapHelper;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class ClerkWorkflow implements Runnable {

	ClerkConfiguration configuration;
	LetterTemplate template;
	Addressee addressee;
	SenderIdentity sender;
	Letter letter;
	
	File targetFile;

	public ClerkWorkflow(ClerkConfiguration configuration, LetterTemplate template, Addressee addressee, SenderIdentity sender, Letter letter) {

		this.configuration = configuration;
		this.template = template;
		this.addressee = addressee;
		this.sender = sender;
		this.letter = letter;

	}

	@Override
	public void run() {

		try {

			/* Step 1: Prepare files */
			ClerkLogger.log("Copying template "+template.getFile().getAbsolutePath()+" to working directory "+configuration.getWorkingDirectory()+"...");
			File latexSource = FileManagement.copyTemplateToWorkingDirectory(configuration, template);
			ClerkLogger.log("Copying template ressources to working directory "+configuration.getWorkingDirectory()+"...");
			FileManagement.copyTemplateRessourcesToWorkingDirectory(configuration, template);

			/* Step 2: Tag Replacement */
			ClerkLogger.log("Replacing tags...");
			DefaultPlaceholderReplacementEngine replacer = new DefaultPlaceholderReplacementEngine();
			replacer.replaceTags(latexSource, ReplaceMapHelper.createReplaceMap(addressee, sender, letter), true);

			/* Step 3: Compile LaTex Source */
			ClerkLogger.log("Executing LaTeX compiler...");
			ClerkCompiler compiler = new GenericCommandCompiler();
			compiler.executeLatexToTarget(configuration, latexSource);

			/* Step 4: Archiving */
			ClerkLogger.log("Archiving...");
			targetFile = FileManagement.archive(configuration, template, addressee.getDisplayName(), letter.getSubject());
			
			/* Step 4: Cleanup */
			ClerkLogger.log("Cleaning up...");
			FileManagement.purgeWorkingDirectory(configuration);

		} catch(Exception excp) {

			throw new RuntimeException(excp);
		}
	}
	
	public File getTargetFile() {
		
		return targetFile;
	}
}
