/**
 * 
 */
package org.evolvis.clerk.core.beans;


/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public interface Address {

	public String getAddressName();
	
	public String getNameAddition();
	
	public String getStreetName();
	
	public String getStreetNumber();
	
	public int getPostalCode();
	
	public String getTown();
	
	public Country getCountry();
}
