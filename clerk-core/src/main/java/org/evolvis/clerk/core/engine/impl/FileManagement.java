/**
 * 
 */
package org.evolvis.clerk.core.engine.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.evolvis.clerk.core.ClerkLogger;
import org.evolvis.clerk.core.beans.LetterTemplate;
import org.evolvis.clerk.core.beans.LetterTemplateRessource;
import org.evolvis.clerk.core.config.ClerkConfiguration;
import org.evolvis.clerk.core.error.ClerkFileManagementException;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class FileManagement {

	public static File copyTemplateToWorkingDirectory(ClerkConfiguration configuration, LetterTemplate template) throws ClerkFileManagementException {

		try {

			if(configuration.getWorkingDirectory().isFile())
				throw new RuntimeException("Desired working directory "+configuration.getWorkingDirectory().getAbsolutePath() + " is a file but must be a directory!");

			if(!configuration.getWorkingDirectory().exists())
				if(!configuration.getWorkingDirectory().mkdirs())
					throw new RuntimeException("Could not create desired working directory "+configuration.getWorkingDirectory().getAbsolutePath());

			File targetFile = new File(configuration.getWorkingDirectory(), template.getFile().getName());

			copyFile(template.getFile(), targetFile);

			return targetFile;

		} catch(IOException excp) {

			throw new ClerkFileManagementException(excp);
		}
	}

	private static void copyFileFromSourceDirToTarget(File sourceDir, File targetDir, String filename) throws IOException {

		File sourceFile = new File(sourceDir, filename);
		File targetFile = new File(targetDir, filename);
		copyFile(sourceFile, targetFile);
	}

	private static void copyFile(File source, File target) throws IOException {

		ClerkLogger.debug("Copying "+source.getAbsolutePath()+" to "+target.getAbsolutePath());

		FileChannel inChannel = new FileInputStream(source).getChannel();
		FileChannel outChannel = new FileOutputStream(target).getChannel();

		try {

			inChannel.transferTo(0, inChannel.size(), outChannel);

		} catch (IOException excp) {

			throw excp;

		} finally {

			if (inChannel != null)
				inChannel.close();

			if (outChannel != null)
				outChannel.close();
		}
	}

	public static void copyTemplateRessourcesToWorkingDirectory(ClerkConfiguration configuration, LetterTemplate template) throws ClerkFileManagementException {

		try {

			if(configuration.getWorkingDirectory().isFile())
				throw new RuntimeException("Desired working directory "+configuration.getWorkingDirectory().getAbsolutePath() + " is a file but must be a directory!");

			if(!configuration.getWorkingDirectory().exists())
				if(!configuration.getWorkingDirectory().mkdirs())
					throw new RuntimeException("Could not create desired working directory "+configuration.getWorkingDirectory().getAbsolutePath());

			List<LetterTemplateRessource> templateRessources = template.getRessources();

			for(LetterTemplateRessource ressource : templateRessources)
				copyFile(ressource.getRessourceFile(), new File(configuration.getWorkingDirectory(), ressource.getRessourceFile().getName()));

		} catch(IOException excp) {

			throw new ClerkFileManagementException(excp);
		}

	}

	public static String readFileContent (File fileToRead) throws Exception { 

		StringBuilder builder = new StringBuilder();
		BufferedReader reader = null;

		try {
			reader = new BufferedReader(new FileReader(fileToRead)); 

			while (reader.ready()) 
				builder.append(reader.readLine()+"\n"); 

		} finally {

			if(reader != null)
				reader.close();
		}
		return builder.toString(); 
	}

	public static void writeFileContent(File fileToWrite, String textToWrite) throws Exception { 

		FileWriter writer = null;

		try {
			writer = new FileWriter(fileToWrite); 

			writer.write(textToWrite); 

			writer.flush();

		} finally {

			writer.close();
		}
	}

	public static File archive(ClerkConfiguration configuration, LetterTemplate template, String addressee, String subject) throws ClerkFileManagementException {

		try {

			// Replace all slashes in subject and addressee (are not allowed in UNIX filenames)
			// FIXME support for other (file)systems!
			String subjectToFilename = subject.replaceAll("/", "⁄");
			String addresseeToFilename = addressee.replaceAll("/", "⁄");

			String pdfFileName = template.getFile().getName().substring(0, template.getFile().getName().lastIndexOf(".")) + ".pdf";

			File sourceFile = new File(configuration.getWorkingDirectory(), pdfFileName);

			File targetDir = new File(configuration.getArchiveDirectory(), new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + " " + addresseeToFilename + " - " + subjectToFilename);

			if(targetDir.isFile())
				throw new RuntimeException("Desired archive directory "+targetDir.getAbsolutePath() + " is a file but must be a directory!");

			if(!targetDir.exists())
				if(!targetDir.mkdirs())
					throw new RuntimeException("Could not create desired archive directory "+targetDir.getAbsolutePath());

			File targetFile = new File(targetDir, subjectToFilename + ".pdf");

			copyFile(sourceFile, targetFile);
			
			return targetFile;

		} catch(Exception excp) {

			throw new ClerkFileManagementException(excp);
		}
	}

	public static void purgeWorkingDirectory(ClerkConfiguration configuration) throws ClerkFileManagementException {
		
		if(configuration.isCleanupDisabled()) {
			
			ClerkLogger.log("Cleaning disabled by configuration.");
			return;
		}
		
		for(File file : configuration.getWorkingDirectory().listFiles())
			if(!file.delete())
				ClerkLogger.log("Could not delete file "+file.getAbsolutePath());
	}
}
