/**
 * 
 */
package org.evolvis.clerk.core.utils;



/**
 * Welcome to Backslash-Paradise!
 * 
 * This class intends to provide helper-methods for dealing with LaTeX,
 * especially with LaTeX-symbol replacement.
 * 
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class LaTeXHelper {

	
	/**
	 * 
	 * Escape special LaTeX Characters
	 * 
	 * @param input a String which may contain special LaTeX characters
	 * @return a String which is guaranteed to not contain special LaTeX characters (They have been escaped)
	 */
	public static String escapeLaTeXCharacters(String input) {
		
		String output = input;
		
		/*
		 * Preparation Phase
		 * 
		 * As some special characters are inter-dependent, we replace them first with custom markers 
		 * and will later insert the actual replacement-symbol. 
		 * 
		 */
		
		// Replace all '$' by '[CLERK_PUT_DOLLAR_SIGN_HERE]'
		output = output.replaceAll("\\$", "[ClerkPutDollarSignHere]");
		
		// Replace all '€' by '[CLERK_PUT_EURO_SIGN_HERE]'
		output = output.replaceAll("€", "[ClerkPutEuroSignHere]");

		// Replace all '^' by '[ClerkPutCirculumHere]'
		output = output.replaceAll("\\^", "[ClerkPutCircumHere]");
		
		/*
		 * Actual Replacement Phase
		 * 
		 */
		
		// Replace all backslashes ('\') by '$\backslash$' -- holy crap, dammit backslashes!
		output = output.replaceAll("\\\\", "\\\\\\$\\\\\\\\backslash\\\\\\$");
		
		// Replace all '%' by '\%'
		output = output.replaceAll("%", "\\\\\\\\%");
		
		// Replace all '&' by '\&'
		output = output.replaceAll("&", "\\\\\\\\&");
		
		// Replace all '_' by '\_'
		output = output.replaceAll("_", "\\\\\\\\_");
		
		// Replace all '~' by '$\sim$'
		output = output.replaceAll("~", "\\\\\\$\\\\\\\\sim\\\\\\$");
		
		// Replace all '#' by '\#'
		output = output.replaceAll("#", "\\\\\\\\#");
		
		// Replace all '{' by '$\{$'
		output = output.replaceAll("\\{", "\\\\\\$\\\\\\\\\\\\\\{\\\\\\$");
		
		// Replace all '}' by '$\}$'
		output = output.replaceAll("\\}", "\\\\\\$\\\\\\\\\\\\\\}\\\\\\$");
		
		// Replace a single linebreak by '\newline'
		// FIXME: Two linebreaks already signal a new paragraph in LaTeX, so they should not be replaced
		output = output.replaceAll("\n", "\\\\\\\\newline{}");
		
		// Replace all '[ClerkPutDollarSignHere]' by '\$'
		output = output.replaceAll("\\[ClerkPutDollarSignHere\\]", "\\\\\\\\\\\\\\$");
		
		// Replace all '€' by '\euro{}'
		output = output.replaceAll("\\[ClerkPutEuroSignHere\\]", "\\\\\\\\euro\\\\\\{\\\\\\}");
		
		// Replace all '[ClerkPutCirculumHere]' by '\textasciicircum{}'
		output = output.replaceAll("\\[ClerkPutCircumHere\\]", "\\\\\\\\textasciicircum\\\\\\{\\\\\\}");
		
		return output;
	}
}
