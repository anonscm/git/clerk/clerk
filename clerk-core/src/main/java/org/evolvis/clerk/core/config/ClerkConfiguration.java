/**
 * 
 */
package org.evolvis.clerk.core.config;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.evolvis.clerk.core.beans.LetterTemplate;
import org.evolvis.clerk.core.beans.SenderIdentity;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
@Root
public class ClerkConfiguration {

	@Element
	String latexToTargetCommand;

	@Element
	String workingDir;

	@Element
	String archiveDir;

	@Element
	String documentViewerCommand;
	
	@Element(required=false)
	Boolean cleanupDisabled;
	
	@ElementList
	List<SenderIdentity> senderIdentities;

	@ElementList
	List<LetterTemplate> letterTemplates;
	
	List<ClerkConfigurationListener> listeners;
	
	public ClerkConfiguration() {

		// initialize default values
		setLatexToTargetCommand("pdflatex -interaction=nonstopmode");
		setWorkingDirectory(new File(System.getProperty("java.io.tmpdir") + File.separator + "clerk-" + System.getProperty("user.name") + File.separator));
		setArchiveDirectory(new File(System.getProperty("user.home")));
		setTargetViewerCommand("okular");
		setSenderIdentities(new ArrayList<SenderIdentity>());
		setLetterTemplates(new ArrayList<LetterTemplate>());
		
		listeners = new ArrayList<ClerkConfigurationListener>();
	}

	public String getLatexToTargetCommand() {

		return latexToTargetCommand;
	}

	public void setLatexToTargetCommand(String latexToTargetCommand) {

		this.latexToTargetCommand = latexToTargetCommand;
	}

	public File getWorkingDirectory() {

		return new File(workingDir);
	}

	public void setWorkingDirectory(File workingDirectory) {

		workingDir = workingDirectory.getAbsolutePath();
	}

	public File getArchiveDirectory() {

		return new File(archiveDir);
	}

	public void setArchiveDirectory(File archiveDirectory) {

		archiveDir = archiveDirectory.getAbsolutePath();
	}

	public String getDocumentViewerCommand() {

		return documentViewerCommand;
	}

	public void setTargetViewerCommand(String documentViewerCommand) {

		this.documentViewerCommand = documentViewerCommand;
	}

	public List<SenderIdentity> getSenderIdentities() {

		return senderIdentities;
	}

	public void setSenderIdentities(List<SenderIdentity> identities) {

		this.senderIdentities = identities;
	}
	
	public List<LetterTemplate> getLetterTemplates() {
		
		return letterTemplates;
	}
	
	public void setLetterTemplates(List<LetterTemplate> letterTemplates) {
		
		this.letterTemplates = letterTemplates;
	}

	public static ClerkConfiguration load() {

		if(getConfigFile().isFile()) {
			try {

				Serializer serializer = new Persister();

				return serializer.read(ClerkConfiguration.class, getConfigFile());

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	
		return new ClerkConfiguration();

	}

	public static File getConfigFile() {

		return new File(System.getProperty("user.home") +  File.separator + ".clerk", "config.xml");
	}

	public void save() {

		try {

			getConfigFile().getParentFile().mkdirs();

			Serializer serializer = new Persister();
			serializer.write(this, getConfigFile());

		} catch (Exception e) {

			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		fireConfigurationChanged();
	}
	
	public void addConfigurationListener(ClerkConfigurationListener listener) {
		
		listeners.add(listener);
	}
	
	private void fireConfigurationChanged() {
		
		for(ClerkConfigurationListener listener : listeners)
			listener.configurationChanged();
	}

	public boolean isCleanupDisabled() {
		
		return Boolean.TRUE.equals(cleanupDisabled);
	}
}
