/**
 * 
 */
package org.evolvis.clerk.core;

import java.util.logging.Logger;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class ClerkLogger {

	private final static Logger logger = Logger.getLogger("[clerk-core]");
	
	public static void log(String message) {
		
		logger.info(message);
	}
	
	public static void log(String message, Throwable e) {
		
		logger.info(message + "\n" + e.getMessage());
	}
	
	public static void debug(String message) {
		
		logger.fine(message);
	}
}
