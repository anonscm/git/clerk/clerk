/**
 * 
 */
package org.evolvis.clerk.core.engine.impl;

import java.io.File;
import java.util.Iterator;
import java.util.Map;

import org.evolvis.clerk.core.ClerkLogger;
import org.evolvis.clerk.core.engine.PlaceholderReplacementEngine;
import org.evolvis.clerk.core.error.ClerkReplacementException;
import org.evolvis.clerk.core.utils.LaTeXHelper;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class DefaultPlaceholderReplacementEngine implements PlaceholderReplacementEngine {

	@Override
	public void replaceTags(File workFile, Map<String, Object> replaceMap, boolean removeRemainingTags) throws ClerkReplacementException {


		if(!workFile.canRead())
			throw new ClerkReplacementException("Cannot read file "+workFile.getAbsolutePath());

		if(!workFile.canWrite())
			throw new ClerkReplacementException("Cannot write file "+workFile.getAbsolutePath());
		
		ClerkLogger.debug("Replacing tags in file "+workFile.getAbsolutePath());

		// Read in work file text content

		String latexSource;

		try {

			latexSource = FileManagement.readFileContent(workFile);

		} catch (Exception excp) {

			throw new ClerkReplacementException("Failed to read LaTex source file", excp);
		}

		Iterator<String> placeholders = replaceMap.keySet().iterator();

		while(placeholders.hasNext()) {

			String placeholder = placeholders.next();
			
			// TODO do not hardcode these, decide it by document-type 
			String latexOutput = placeholder.startsWith("LETTER-") ?
					LaTeXHelper.escapeLaTeXCharacters(replaceMap.get(placeholder).toString()) :
						replaceMap.get(placeholder).toString();
					
			
			ClerkLogger.debug("Replacing _"+placeholder+"_ by "+latexOutput);
			latexSource = latexSource.replaceAll("_"+placeholder+"_", latexOutput);
		}
		
		// If requested, remove remaining tags from source-file
		if(removeRemainingTags)
			latexSource = latexSource.replaceAll("_(\\w|-)*_", "");

		// Write text back to work file

		try {

			FileManagement.writeFileContent(workFile, latexSource);

		} catch (Exception excp) {

			throw new ClerkReplacementException("Failed to read LaTex source file", excp);
		}
	}

	
}
