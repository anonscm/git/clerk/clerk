/**
 * 
 */
package org.evolvis.clerk.core.engine.impl;

import java.io.File;

import org.evolvis.clerk.core.ClerkLogger;
import org.evolvis.clerk.core.config.ClerkConfiguration;
import org.evolvis.clerk.core.engine.ClerkCompiler;
import org.evolvis.clerk.core.error.ClerkCompilerException;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class GenericCommandCompiler implements ClerkCompiler {

	@Override
	public void executeLatexToTarget(ClerkConfiguration configuration, File latexSource) throws ClerkCompilerException {

		File workingDir = configuration.getWorkingDirectory();
		String latexToTargetCmd = configuration.getLatexToTargetCommand();

		try {
			
			ClerkLogger.log("starting compiler command "+latexToTargetCmd);

			Process process = Runtime.getRuntime().exec(latexToTargetCmd + " " +latexSource.getAbsolutePath(), new String[] {}, workingDir);

			ClerkLogger.log("command "+latexToTargetCmd+" exited with value "+process.waitFor());
			
			
			if(process.exitValue() != 0)
				throw new ClerkCompilerException(latexToTargetCmd + " returned " + process.exitValue()); 

		} catch (Exception e) {

			throw new ClerkCompilerException(e);
		}
	}
}