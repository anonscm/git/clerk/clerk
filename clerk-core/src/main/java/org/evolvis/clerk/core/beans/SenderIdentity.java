/**
 * 
 */
package org.evolvis.clerk.core.beans;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;


/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class SenderIdentity extends AbstractClerkBean implements Address {
	
	@Attribute
	String identityName;
	
	@Element
	@ClerkPlaceholder("SENDER-NAME")
	String senderName;

	@Element(required=false)
	@ClerkPlaceholder("SENDER-NAME-ADDITION")
	String senderNameAddition;
	
	@Element
	@ClerkPlaceholder("SENDER-STREET-NAME")
	String senderStreetName;
	
	@Element
	@ClerkPlaceholder("SENDER-STREET-NUMBER")
	String senderStreetNumber;
	
	@Element
	@ClerkPlaceholder("SENDER-POSTAL-CODE")
	Integer senderPostalCode;
	
	@Element
	@ClerkPlaceholder("SENDER-TOWN")
	String senderTown;
	
	@Element
	@ClerkPlaceholder("SENDER-COUNTRY")
	Country senderCountry;
	
	@Element(required=false)
	@ClerkPlaceholder("SENDER-SIGNATURE")
	String senderSignature;
	
	@Element(required=false)
	@ClerkPlaceholder("SENDER-PLACE")
	String senderPlace;
	
	@Element(required=false)
	@ClerkPlaceholder("SENDER-PHONE")
	String senderPhone;
	
	@Element(required=false)
	@ClerkPlaceholder("SENDER-FAX")
	String senderFax;
	
	@Element(required=false)
	@ClerkPlaceholder("SENDER-EMAIL")
	String senderEmail;
	
	@Element(required=false)
	@ClerkPlaceholder("SENDER-WWW")
	String senderWWW;
	
	public SenderIdentity() {
		
		
	}
	
	public SenderIdentity(String identityName) {
		
		this.identityName = identityName;
	}
	
	public SenderIdentity(String identityName, Address address, String companyName, String creationPlace, String signature, String phone, String fax, String email, String www) {
		
		this(identityName, address.getAddressName(), companyName, address.getStreetName(), address.getStreetNumber(), address.getPostalCode(),
				address.getTown(), address.getCountry(), signature, creationPlace, phone, fax,
				email, www);
	}
	
	public SenderIdentity(String identityName, String name, String nameAddition, String streetName, String streetNumber, int postalCode,
			String town, Country country, String signature, String creationPlace, String phone, String fax,
			String email, String www) {
		
		this(identityName);
		this.senderName = name;
		this.senderNameAddition = nameAddition;
		this.senderStreetName = streetName;
		this.senderStreetNumber = streetNumber;
		this.senderPostalCode = postalCode;
		this.senderTown = town;
		this.senderCountry = country;
		this.senderSignature = signature;
		this.senderPlace = creationPlace;
		this.senderPhone = phone;
		this.senderFax = fax;
		this.senderEmail = email;
		this.senderWWW = www;
	}
	
	public String getIdentityName() {
		
		return identityName;
	}
	
	public void setIdentityName(String identityName) {
		
		this.identityName = identityName;
	}
	
	@Override
	public String getAddressName() {
		
		return senderName;
	}

	@Override
	public int getPostalCode() {
		
		return senderPostalCode;
	}

	@Override
	public String getStreetName() {
		
		return senderStreetName;
	}

	@Override
	public String getStreetNumber() {
		
		return senderStreetNumber;
	}

	@Override
	public String getTown() {
		
		return senderTown;
	}
	
	public String getCreationPlace() {
		
		return senderPlace;
	}
	
	public String getSignature() {
		
		return senderSignature;
	}
	
	public String getPhone() {
		
		return senderPhone;
	}
	
	public String getFax() {
		
		return senderFax;
	}
	
	public String getEmail() {
		
		return senderEmail;
	}
	
	public String getWWW() {
		
		return senderWWW;
	}

	@Override
	public Country getCountry() {
		
		return senderCountry;
	}

	@Override
	public String getDisplayName() {
		
		return getIdentityName();
	}

	@Override
	public String getNameAddition() {
		
		return senderNameAddition;
	}
}
