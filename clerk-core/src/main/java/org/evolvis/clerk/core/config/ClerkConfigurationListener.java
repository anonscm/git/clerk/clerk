/**
 * 
 */
package org.evolvis.clerk.core.config;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public interface ClerkConfigurationListener {

	public void configurationChanged();
}
