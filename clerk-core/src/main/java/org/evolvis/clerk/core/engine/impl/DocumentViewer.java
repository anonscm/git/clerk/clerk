/**
 * 
 */
package org.evolvis.clerk.core.engine.impl;

import java.io.File;

import org.evolvis.clerk.core.ClerkLogger;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class DocumentViewer {

	String documentViewerCommand;
	
	public DocumentViewer(String documentViewerCommand) {
		
		this.documentViewerCommand = documentViewerCommand;
	}
	
	public void showDocument(File document) {
		
		ClerkLogger.log("Using "+documentViewerCommand+" to display file \""+document.getAbsolutePath()+"\"");
		
		ClerkLogger.debug("Executing command "+documentViewerCommand + " \"" + document.getAbsolutePath() + "\"");
		
		try {
			
			Process process = Runtime.getRuntime().exec(new String[] { documentViewerCommand, document.getAbsolutePath() });
			
			if(process.waitFor() != 0)
				throw new RuntimeException("Document viewer exited with error "+process.exitValue());
			
		} catch (Exception e) {
			
			throw new RuntimeException("Could not launch document viewer\nCommand: "+documentViewerCommand, e);
		}
	}
}
