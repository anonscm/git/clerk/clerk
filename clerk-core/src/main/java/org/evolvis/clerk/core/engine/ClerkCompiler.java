/**
 * 
 */
package org.evolvis.clerk.core.engine;

import java.io.File;

import org.evolvis.clerk.core.config.ClerkConfiguration;
import org.evolvis.clerk.core.error.ClerkCompilerException;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public interface ClerkCompiler {

	public void executeLatexToTarget(ClerkConfiguration configuration, File latexSource) throws ClerkCompilerException;
}
