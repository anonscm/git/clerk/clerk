/**
 * 
 */
package org.evolvis.clerk.core.error;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class ClerkFileManagementException extends Exception {

	public ClerkFileManagementException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ClerkFileManagementException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public ClerkFileManagementException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public ClerkFileManagementException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

}
