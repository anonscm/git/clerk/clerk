/**
 * 
 */
package org.evolvis.clerk.core.beans;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.evolvis.clerk.core.ClerkLogger;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public abstract class AbstractClerkBean implements Cloneable {

	public List<String> getAllTags() {
		
		return getTags(false);
	}
	
	public List<String> getAllDefinedTags() {
		
		return getTags(true);
	}
	
	private List<String> getTags(boolean onlyDefinedTags) {
		
		List<String> tags = new ArrayList<String>();
		
		Field[] fields = getClass().getDeclaredFields();
		
		for(Field field : fields) {
			
			try {
				
				if(field.isAnnotationPresent(ClerkPlaceholder.class) && (!onlyDefinedTags || field.get(this) != null))
					tags.add(field.getAnnotation(ClerkPlaceholder.class).value());
				
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return tags;
	}
	
	public Object getValueForPlaceholder(String key) {
		
		try {
			
			Field[] fields = this.getClass().getDeclaredFields();
			
			Field fieldForPlaceholder = null;
			
			for(Field field : fields) {
				if(field.isAnnotationPresent(ClerkPlaceholder.class) && field.getAnnotation(ClerkPlaceholder.class).value().equals(key)) {
					fieldForPlaceholder = field;
					break;
				}
			}
			
			if(fieldForPlaceholder == null) {
				
				ClerkLogger.log("There is no such field \""+key+"\"");
				return null;
			}
				
			
			Object value = fieldForPlaceholder.get(this);
			
			if(value == null)
				return null;
			
			return value;
			
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public abstract String getDisplayName();

	@Override
	public synchronized String toString() {
		
		return getDisplayName();
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}
}
