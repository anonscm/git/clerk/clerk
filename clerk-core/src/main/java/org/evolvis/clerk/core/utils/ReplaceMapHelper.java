/**
 * 
 */
package org.evolvis.clerk.core.utils;

import java.util.HashMap;
import java.util.Map;

import org.evolvis.clerk.core.ClerkLogger;
import org.evolvis.clerk.core.beans.AbstractClerkBean;
import org.evolvis.clerk.core.beans.Addressee;
import org.evolvis.clerk.core.beans.Letter;
import org.evolvis.clerk.core.beans.SenderIdentity;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class ReplaceMapHelper {

	public static Map<String, Object> createReplaceMap(Addressee addressee, SenderIdentity sender, Letter text) {
		
		Map<String, Object> replaceMap = new HashMap<String, Object>();
		
		ClerkLogger.debug("Creating replace map");
		
		// If origin country is the same as the addressee country, remove placeholder from replace-map
		// country should only be in address if it is different from the origin country.
		addressee.generateAddress(
				!sender.getCountry().equals(addressee.getCountry()));
		
		copyBeanToMap(replaceMap, addressee);
		copyBeanToMap(replaceMap, sender);
		copyBeanToMap(replaceMap, text);
		
		return replaceMap;
	}
	
	private static void copyBeanToMap(Map<String, Object> replaceMap, AbstractClerkBean bean) {
		
		for(String tag : bean.getAllDefinedTags())
			replaceMap.put(tag, bean.getValueForPlaceholder(tag));
	}
}
