/**
 * 
 */
package org.evolvis.clerk.core.error;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class ClerkReplacementException extends Exception {

	public ClerkReplacementException() {
		
	}
	
	public ClerkReplacementException(String message, Throwable throwable) {
		
		super(message, throwable);
	}
	
	public ClerkReplacementException(String message) {
		
		super(message);
	}
	
	public ClerkReplacementException(Throwable throwable) {
		
		super(throwable);
	}
}
