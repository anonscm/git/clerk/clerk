/**
 * 
 */
package org.evolvis.clerk.core.beans;

import java.util.Locale;

import org.simpleframework.xml.Attribute;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class Country extends AbstractClerkBean implements Comparable<Country> {

	/**
	 * An upper-case, two-letter code as defined by ISO-3166,
	 * specifying the country represented by this object.
	 */
	@Attribute
	String countryIsoCode;
	
	/**
	 * The corresponding Locale object
	 */
	Locale locale;
	
	public Country() {
		
		
	}
	
	public Country(String countryIsoCode) {
		
		this.countryIsoCode = countryIsoCode;
		
		getLocale();
				
	}
	
	public Country(Locale locale) {
		
		if(locale == null)
			throw new IllegalArgumentException("Input value 'null' is forbidden");
		
		countryIsoCode = locale.getCountry();
		this.locale = locale;
	}
	
	private Locale getLocale() {
		
		if(locale == null) {
			
			Locale[] locales = Locale.getAvailableLocales();
			
			for(Locale locale :  locales) {
				if(locale.getCountry().equals(countryIsoCode)) {
					
					this.locale = locale;
					break;
				}
			}
			
			if(this.locale == null)
				throw new IllegalArgumentException("There is no country with ISO-3166 code '"+countryIsoCode+"'");
		}
		
		return locale;
	}

	/**
	 * Returns the String which will be inserted into the letter-document.
	 * 
	 * This method returns the country-name upper-case.
	 */
	@Override
	public String toString() {
		
		return getLocale().getDisplayCountry().toUpperCase();
	}
	
	public String getIsoCode() {
		
		return countryIsoCode;
	}

	/**
	 * 
	 * Returns the String which will be shown in the UI.
	 */
	@Override
	public String getDisplayName() {
		
		return getLocale().getDisplayCountry();
	}

	@Override
	public int compareTo(Country o) {
		
		return getDisplayName().compareTo(o.getDisplayName());
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if(obj == null)
			return false;
		
		if(!(obj instanceof Country))
			return false;
		
		return this.getIsoCode().equals(((Country)obj).getIsoCode());
	}

	public static Country getSystemCountry() {
		
		return new Country(Locale.getDefault().getCountry());
	}
}
